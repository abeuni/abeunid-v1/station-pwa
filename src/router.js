import Vue from 'vue'
import Router from 'vue-router'
import LocationSelector from './views/LocationSelector.vue'
import QrScanner from './views/QrScanner'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: 'location-selector'
    },
    {
      path: '/location-selector',
      name: 'location-selector',
      component: LocationSelector
    },
    {
      path: '/qr-scanner/:location',
      name: 'qr-scanner',
      component: QrScanner
    }
  ]
})
